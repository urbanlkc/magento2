<?php
namespace MGS\Mpanel\Controller\Adminhtml\Mpanel\Install;

/**
 * Interceptor class for @see \MGS\Mpanel\Controller\Adminhtml\Mpanel\Install
 */
class Interceptor extends \MGS\Mpanel\Controller\Adminhtml\Mpanel\Install implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Backend\App\Action\Context $context, \Magento\Config\Model\Config\Factory $configFactory, \Magento\Framework\Filesystem $filesystem, \Magento\Framework\Xml\Parser $parser, \Magento\Framework\Stdlib\StringUtils $string, \MGS\Mpanel\Helper\Data $_themeHelper)
    {
        $this->___init();
        parent::__construct($context, $configFactory, $filesystem, $parser, $string, $_themeHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }
}
